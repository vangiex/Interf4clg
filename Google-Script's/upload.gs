// Author : Er. Rahul Rathi


function doGet(e)
{
  return HtmlService.createHtmlOutputFromFile('Upload.html');
}

function Auth0()
{
  var groups = ContactsApp.getContactGroups();
  for(var i=0 ; i<groups.length ; i++)
  {
    var group = ContactsApp.getContactGroup(groups[i].getName());
    var contacts = group.getContacts();
    for( var j=0 ; j<contacts.length ; j++)
    {
      var email = contacts[j].getEmails();
      if( email[0].getAddress() == Session.getActiveUser().getEmail())
      {

        return email[0].getAddress();
      }
    }
  }
  return null;
}


function uploadFiles(form)
{
  if(Auth0()!=null)
  {

    try {

    var dropbox = "C.S.E Upload's";
    var folder, folders = DriveApp.getFoldersByName(dropbox);

    if (folders.hasNext()) {
      folder = folders.next();
    } else {
      folder = DriveApp.createFolder(dropbox);
    }

    f2=folder.getFoldersByName(form.year);
    if (f2.hasNext()) {
      folder = f2.next();
    } else {
      folder = folder.createFolder(form.year);
    }

    sem= form.sem.toString();
    sem= "Sem " + sem;
    f2=folder.getFoldersByName(sem);
    if (f2.hasNext()) {
      folder = f2.next();
    } else {
      folder = folder.createFolder(sem);
    }

    f2=folder.getFoldersByName(form.sub);
    if (f2.hasNext()) {
      folder = f2.next();
    } else {
      folder = folder.createFolder(form.sub);
    }

    var blob = form.file;
    var file = folder.createFile(blob);
    file.setDescription("Uploaded by " + form.enroll);
    return "File uploaded successfully ";

  } catch (error) {

    return error.toString();
  }
  }
  else
  {
    return("Auth0 error!");
  }
}
