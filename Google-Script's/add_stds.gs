// Author : Er. Rahul Rathi


function doGet(e)
{
  return HtmlService.createHtmlOutputFromFile('add_stds.html');
}

function Auth1()
{
  var group = ContactsApp.getContactGroup("Dept");
  var contacts = group.getContacts();
    for( var j=0 ; j<contacts.length ; j++)
    {
      var email = contacts[j].getEmails();
      if( email[0].getAddress() == Session.getActiveUser().getEmail())
      {
        return email[0].getAddress();
      }
    }
  return null;
}


function add_stds(form)
{
  if(Auth1()==null)
  {
    return "Auth1 error" ;
  }
  else
  {
    try {
  var group  = ContactsApp.getContactGroup(form.year);
  var grp;
     if (group != null )
     {
       grp=group;
     } else {
         grp = ContactsApp.createContactGroup(form.year);
       }
  grp.addContact(ContactsApp.createContact(form.name , form.last , form.email));

  return "ok";
  } catch (error) {

    return error.toString();
  }
  }
}
